import argparse
import json
import os
import tsa_connector


def load_json_file(path_to_file: str):
    abs_path = os.path.abspath(path_to_file)
    with open(abs_path, 'r') as file:
        return json.load(file)

def write_json_file(json_data: dict, path_to_file: str) -> None:
    abs_path = os.path.abspath(path_to_file)
    if os.path.exists(abs_path):
        raise Exception("File '" + abs_path + "' already exists. Please choose another file")

    with open(abs_path, 'w') as file:
        json.dump(json_data, file, indent=4, sort_keys=True)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Connect to TSA.')
    sub_parser = parser.add_subparsers(dest="output")

    # Setup parser for TSA post request
    post_claims = sub_parser.add_parser("post-claims", help="Send claims to TSA.")
    post_claims.add_argument('--url', type=str, required=True,
                        help='URL to TSA.')
    post_claims.add_argument('--file', type=str, required=True,
                        help='Path to json file with claims.')
    post_claims.add_argument('--cache-key', type=str, required=True,
                             help='Key for json file in TSA cache.')

    # Setup parser for TSA post request
    get_sd = sub_parser.add_parser("get-sd", help="Request signed self-description from TSA.")
    get_sd.add_argument('--url', type=str, required=True,
                        help='URL to TSA.')
    get_sd.add_argument('--file', type=str, required=True,
                        help='Path to file where self-description should be stored.')

    args = parser.parse_args()
    #    , _ = parser.parse_known_args()
    if args.output == "post-claims":
        claims = load_json_file(args.file)
        tsa_connector.post_claims(claims, args.url, args.cache_key)
    elif args.output == "get-sd":
        json_response = tsa_connector.get_self_description(args.url)
        write_json_file(json_response, args.file)


