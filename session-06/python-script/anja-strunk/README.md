# Python Script to connect GXFS TSA


## Send claims
Send claims (output Self-Description Creation Wizard in JSON-LD) to TSA:

``python3 cli.py post-claims --url <TSA-POST-URL> --file <PATH-TO-FILE-WITH-CLaiMS> --cache-key=<CACHE-KEY>``

Sample:

``python3 cli.py post-claims --url=https://gaiax.vereign.com/tsa/cache/v1/cache --file=../../CompanyProfileProvider-sample.json --cache-key=Provider``


## Get Self-Description

``python3 cli.py get-sd --url <TSA-GET-URL> --file <PATH-TO-FILE-TO-STORE_SD_IN>``

Sample:

``python3 cli.py get-sd --url=https://gaiax.vereign.com/tsa/infohub/v1/export/Provider --file=self-description.json``
