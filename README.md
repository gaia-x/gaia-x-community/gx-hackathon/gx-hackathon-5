# GX-Hackathon 5

The project for the 5th Gaia-X Hackathon event of the Gaia-X Open-Source Software Community taking place on **September 26-27, 2022**, as **online event open for everybody**.

Register now for the Gaia-X Hackathon: [https://gaia-x.eu/event/gaia-x-hackathon-5/](https://gaia-x.eu/event/gaia-x-hackathon-5/)

![Banner](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/raw/main/Marketing%20and%20Communications/GX-Hackathon_5_Web_Banner.jpg)

# Intro 

We are delighted to invite you, the Gaia-X open-source software community, to the 5th Gaia-X Hackathon. 

The Gaia-X Hackathon is a two days event with each day being crowned with a presentation session of showcases that leverage components and concepts of the Gaia-X architecture. If you would like to participate in the Hackathon, please **[register here](https://online2.superoffice.com/Cust26633/CS/scripts/customer.fcgi?action=formFrame&formId=F-U06hqXqB)** and participate in the Gaia-X Open-Source Software Community [Weekly each Thursday from 09:00 CET to 10:00 CET here](https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjJlZDE5ZTctOTY4Ni00Y2FkLWE5OTAtZDg2OGQ3YWRiZGIz%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

The Gaia-X Hackathon is organized as a community event in regular intervals by the Gaia-X AISBL, the Gaia-X Open-Source Software (OSS) Community Working Group and eco – Association of the Internet Industry.

# Getting in Touch

For discussions related to the Hackathon you can use the mailing list of the MVG/Piloting Open Work Package. You can subscribe to it by navigating to [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu/). After approval you'll be able to communicate through the mailing list oss-community@list.gaia-x.eu.

**If you would like to participate in the organization and/or would like to get updates on the organization, please join our call on:**
- Thursday 0900-1000 CET - join: [MS Teams Link](https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjJlZDE5ZTctOTY4Ni00Y2FkLWE5OTAtZDg2OGQ3YWRiZGIz%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

**You can also join our chat on Slack and Matrix:**
- Slack: [https://join.slack.com/t/gaia-xworkspace/shared_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o_sA](https://join.slack.com/t/gaia-xworkspace/shared_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o_sA)
- Matrix: [https://matrix.to/#/#gaia-x-hackathon:matrix.org](https://matrix.to/#/#gaia-x-hackathon:matrix.org)

# Registration, Schedule and Further Information

The **Registration** is available [here](https://online2.superoffice.com/Cust26633/CS/scripts/customer.fcgi?action=formFrame&formId=F-U06hqXqB). Without registration you will not be able to attend the event.

You'll find the **schedule** and wiki right [here](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/home) on the main Wiki page.

Please find and add session proposals here: **[Hackathon 5 Proposals](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/Hackathon-5-Proposals)**
