import * as data from "./data";
import { CloudService, Compliance, Control, Evidence, ListCloudServicesResponse, MetricConfiguration, MonitoringStatus, ServiceConfiguration, StartMonitoringResponse } from "./model";
import * as REST from "./newREST";

export enum ComplianceStatus {
    Compliant = 1,
    NonComplaint,
    Pending
}

/**
 * List cloud service configurations.
 * 
 * @param serviceId the cloud service id
 * @returns the list of service configurations
 */
export async function listServiceConfigurations(serviceId: string): Promise<ServiceConfiguration[]> {
    if (data.getTestdata()) {
        return Promise.resolve([])
    } else {
        return REST.listServiceConfigurations(serviceId).then(res => res.configurations);
    }
}

/**
 * Configures a cloud service.
 * 
 * @param serviceId the cloud service id
 * @param configurations the configurations
 */
export async function configureCloudService(serviceId: string, configurations: ServiceConfiguration[]) {
    if (data.getTestdata()) {
        return Promise.resolve([])
    } else {
        return REST.configureCloudService(serviceId, {
            configurations: configurations
        })
    }
}

/**
 * Lists all cloud services that are registered in the system.
 * 
 * @returns a list of cloud services
 */
export async function listCloudServices(): Promise<CloudService[]> {
    if (data.getTestdata()) {
        return Promise.resolve([
            { name: 'Service-1', id: '1', requirements: { requirementIds: [] } },
            { name: 'Service-2', id: '2', requirements: { requirementIds: [] } },
            { name: 'Service-3', id: '3', requirements: { requirementIds: [] } },
            { name: 'Service-4', id: '4', requirements: { requirementIds: [] } }]);
    } else {
        return REST.listCloudServices().then(res => res.services);
    }
}

/**
 * Returns a specific cloud services.
 * 
 * @param serviceId the cloud service id
 * @returns the cloud service
 */
export async function getCloudService(serviceId: string): Promise<CloudService> {
    if (data.getTestdata()) {
        return Promise.resolve(
            { name: 'Service-1', id: '1', requirements: { requirementIds: [] } },
        );
    } else {
        return REST.getCloudService(serviceId);
    }
}


/**
 * Lists all cloud services that are registered in the system for a specfic date.
 * 
 * @returns a list of cloud services
 */
export async function listCloudServicesForDay(day): Promise<CloudService[]> {
    if (data.getTestdata()) {
        return Promise.resolve([
            { name: 'Service-1', id: '1' },
            { name: 'Service-2', id: '2' },
            { name: 'Service-3', id: '3' },
            { name: 'Service-4', id: '4' }]);
    } else {
        return REST.listCloudServicesForDay(day).then(res => res.services);
    }
}

/**
 * Registeres a new cloud service.
 * 
 * @param name the name of the new cloud service
 * @returns the created cloud service
 */
export async function registerCloudService(name: string, description?: string | undefined): Promise<CloudService> {
    if (data.getTestdata()) {
        return Promise.resolve({
            id: "5",
            name: name,
            description: description,
            requirements: { requirementIds: [] }
        });
    } else {
        return REST.registerCloudService({
            name: name,
            description: description,
        })
    }
}

/**
 * Removes a certain cloud service.
 * 
 * @param serviceId the cloud service id
 */
export async function removeCloudService(serviceId: string): Promise<void> {
    if (data.getTestdata()) {
        return Promise.resolve();
    } else {
        return REST.removeCloudService(serviceId);
    }
}

/**
 * Updates a certain cloud service.
 */
export async function updateCloudService(serviceId: string, service: CloudService): Promise<CloudService> {
    if (data.getTestdata()) {
        return Promise.resolve(service);
    } else {
        return REST.updateCloudService(serviceId, service);
    }
}

/**
 * Returns the monitoring status of a cloud service.
 * 
 * @param serviceId the cloud service id
 * @returns the list of monitored controls. Empty, if monitoring is turned off. 
 */
export async function getMonitoringStatus(serviceId: string): Promise<MonitoringStatus> {
    if (data.getTestdata()) {
        let controlIds = ["Control-1", "Control-2"];
        let random = controlIds.sort(() => .5 - Math.random()).slice(0, Math.random() * controlIds.length);

        return Promise.resolve({
            serviceId: serviceId,
            controlIds: random,
        })
    } else {
        return REST.getMonitoringStatus(serviceId);
    }
}

/**
 * Triggers the monitoring of certain controls for a particular cloud service.
 * 
 * @param serviceId the cloud service id
 * @param controlIds the controls to monitor
 * @returns the current monitoring status immediately after the start
 */
export async function startMonitoring(serviceId: string, controlIds: string[]): Promise<MonitoringStatus> {
    if (data.getTestdata()) {
        return Promise.resolve({
            serviceId: serviceId,
            controlIds: controlIds,
        })
    } else {
        return REST.startMonitoring(serviceId, {
            serviceId: serviceId,
            controlIds: controlIds
        }).then(res => res.status);
    }
}

/**
 * Lists compliance results for a specific service.
 * 
 * @param serviceId the cloud service id
 * @returns a list of compliance results
 */
export async function listCompliance(serviceId: string): Promise<Compliance[]> {
    if (data.getTestdata()) {
        return Promise.resolve([
            { id: "1", serviceId: "1", controlId: "Control-1", status: true, evidenceId: "5", time: new Date().toISOString(), evaluations: [] },
            { id: "2", serviceId: "1", controlId: "Control-2", status: true, evidenceId: "5", time: new Date().toISOString(), evaluations: [] },
            { id: "3", serviceId: "2", controlId: "Control-1", status: false, evidenceId: "5", time: new Date().toISOString(), evaluations: [] },
            { id: "4", serviceId: "2", controlId: "Control-2", status: true, evidenceId: "5", time: new Date().toISOString(), evaluations: [] },
            { id: "5", serviceId: "3", controlId: "Control-1", status: false, evidenceId: "5", time: new Date().toISOString(), evaluations: [] },
            { id: "6", serviceId: "3", controlId: "Control-2", status: false, evidenceId: "5", time: new Date().toISOString(), evaluations: [] },
            { id: "7", serviceId: "4", controlId: "Control-1", status: true, evidenceId: "5", time: new Date().toISOString(), evaluations: [] },
            { id: "8", serviceId: "4", controlId: "Control-2", status: false, evidenceId: "5", time: new Date().toISOString(), evaluations: [] },
        ])
    } else {
        return REST.listCompliance(serviceId).then(res => res.complianceResults);
    }
}

/**
 * Lists compliance results for a specific service.
 * 
 * @param serviceId the cloud service id
 * @param day date for the results
 * @returns a list of compliance results
 */
export async function listComplianceForDay(serviceId: string, day: number): Promise<Compliance[]> {
    if (data.getTestdata()) {
        return Promise.resolve([
            { id: "1", serviceId: "1", controlId: "Control-1", status: true, evidenceId: "5", time: new Date().toISOString(), evaluations: [] },
            { id: "2", serviceId: "1", controlId: "Control-2", status: true, evidenceId: "5", time: new Date().toISOString(), evaluations: [] },
            { id: "3", serviceId: "2", controlId: "Control-1", status: false, evidenceId: "5", time: new Date().toISOString(), evaluations: [] },
            { id: "4", serviceId: "2", controlId: "Control-2", status: true, evidenceId: "5", time: new Date().toISOString(), evaluations: [] },
            { id: "5", serviceId: "3", controlId: "Control-1", status: false, evidenceId: "5", time: new Date().toISOString(), evaluations: [] },
            { id: "6", serviceId: "3", controlId: "Control-2", status: false, evidenceId: "5", time: new Date().toISOString(), evaluations: [] },
            { id: "7", serviceId: "4", controlId: "Control-1", status: true, evidenceId: "5", time: new Date().toISOString(), evaluations: [] },
            { id: "8", serviceId: "4", controlId: "Control-2", status: false, evidenceId: "5", time: new Date().toISOString(), evaluations: [] },
        ])
    } else {

        // Remove all results which are not from requested day
        const results = await REST.listCompliance30Days(serviceId).then(res => res.complianceResults);
        var d = new Date();
        d.setDate(d.getDate() - day);
        const date =
            d.getFullYear() +
            "-" +
            (d.getMonth() + 1) +
            "-" +
            d.getDate();

        var filteredResults = results.filter(data => data.time.includes(date));
        return filteredResults;
    }
}

/**
 * Lists compliance results grouped by the control.
 * 
 * @param serviceId the cloud service id
 * @returns a map of controls and compliance results
 */
export async function listGroupedCompliance(serviceId: string): Promise<Map<string, Compliance[]>> {
    const results = await listCompliance(serviceId);

    // Group the results by control
    var groupedResult = results.reduce((all: Map<string, Compliance[]>, current: Compliance) => {
        let controls: Compliance[] = all.get(current.controlId) ?? [];
        controls.push(current);
        all.set(current.controlId, controls);

        return all;
    }, new Map<string, Compliance[]>());

    return groupedResult;
}

/**
 * Lists compliance results grouped by the control.
 * 
 * @param serviceId the cloud service id
 * @param day date for the results
 * @returns a map of controls and compliance results
 */
export async function listGroupedComplianceForDay(serviceId: string, day: number): Promise<Map<string, Compliance[]>> {
    const results = await listComplianceForDay(serviceId, day);

    // Group the results by control
    var groupedResult = results.reduce((all: Map<string, Compliance[]>, current: Compliance) => {
        let controls: Compliance[] = all.get(current.controlId) ?? [];
        controls.push(current);
        all.set(current.controlId, controls);

        return all;
    }, new Map<string, Compliance[]>());

    return groupedResult;
}

/**
 * Retrieves the latest (in time) compliance results grouped by control ID.
 * 
 * @param groupedResults the compliance results grouped by control ID
 * @returns the latest (in time) compliance results grouped by control ID
 */
export function retrieveLatestCompliance(groupedResults: Map<string, Compliance[]>): Map<string, Compliance> {
    let latest = new Map<string, Compliance>();

    // The first element is always the "latest"
    groupedResults.forEach((value, key) => latest.set(key, value[0]));

    return latest;
}

/**
 * Checks, if a service is compliance depnding on the compliance status of its controls.
 * 
 * @param latest the latest compliance results grouped by control
 * @returns the compliance status
 */
export function isServiceCompliant(latest: Map<string, Compliance>): ComplianceStatus {
    let statuses = Array.from(latest.values()).map(isControlCompliant)

    // If we have no results that are either compliant or non-compliant, we are still pending
    if (!statuses.find((s) => s == ComplianceStatus.Compliant || s == ComplianceStatus.NonComplaint)) {
        return ComplianceStatus.Pending;
    }

    // If any control is non-compliant, the whole service is non-compliant
    if (statuses.find((s) => s == ComplianceStatus.NonComplaint)) {
        return ComplianceStatus.NonComplaint;
    }

    // Otherwise, the service is compliant
    return ComplianceStatus.Compliant;
}

/**
 * Checks, if a control is compliant. This is the case when the status is true AND if there are any evaluation results.
 * In the future phase 2 specification, we will deal with this differently and convert the status field into a enum.
 *
 * If the status is true and there are no evaluation results (yet), the status is pending, otherwise it is
 * non-compliant.
 *
 * @param result the compliance compliance result
 * @returns the compliance status
 */
export function isControlCompliant(result: Compliance): ComplianceStatus {
    if (result.status && result.evaluations.length > 0) {
        return ComplianceStatus.Compliant;
    } else if (result.status && result.evaluations.length == 0) {
        return ComplianceStatus.Pending;
    } else {
        return ComplianceStatus.NonComplaint;
    }
}

/**
 * Retrieves the compliance result for a particular service and control.
 * 
 * @param serviceId the cloud service id
 * @param controlId the control id
 * @returns the compliance result
 */
export async function getCompliance(serviceId: string, controlId: string): Promise<Compliance> {
    if (data.getTestdata()) {
        return Promise.resolve(
            { id: "1", serviceId: "1", controlId: "Control-1", status: true, evidenceId: "5", time: new Date().toISOString(), evaluations: [] }
        )
    } else {
        return REST.getCompliance(serviceId, controlId);
    }
}

/**
 * Lists all controls.
 * 
 * @returns returns a list of all controls
 */
export async function listControls(): Promise<Control[]> {
    if (data.getTestdata()) {
        return Promise.resolve([{ id: "Control-1", name: 'Control1', metrics: [] }, { id: "Control-2", name: 'Control2', metrics: [] }]);
    }
    else {
        return REST.listControls();
    }
}

/**
 * Retrieves a particular evidence.
 * 
 * @param controlId the evidence id
 * @result the evidence
 */
export async function getEvidence(evidenceId: string): Promise<Evidence> {
    if (data.getTestdata()) {
        return Promise.resolve({ id: "1", value: {}, targetResource: "1", targetService: "1", toolId: "2", gatheredAt: new Date().toISOString() })
    } else {
        return REST.getEvidence(evidenceId);
    }
}

/**
 * Retrieves a particular metric configuration.
 */
export async function getMetricConfiguration(serviceId: string, metricId: string): Promise<MetricConfiguration> {
    if (data.getTestdata()) {
        return Promise.resolve({ operator: "==", targetValue: 5, isDefault: true, serviceId: "1", metricId: "1" })
    } else {
        return REST.getMetricConfiguration(serviceId, metricId);
    }
}

export async function updateMetricConfiguration(serviceId: string, metricId: string, config: MetricConfiguration): Promise<MetricConfiguration> {
    if (data.getTestdata()) {
        return Promise.resolve({ operator: "==", targetValue: 5, isDefault: true, serviceId: "1", metricId: "1" })
    } else {
        return REST.updateMetricConfiguration(serviceId, metricId, config);
    }
}

export interface ServiceComplianceData {
    latest: Map<string, Compliance>
    latestDate?: Date,
    chartdata: { labels: string[], datasets: { backgroundColor: string[], data: number[] }[] }
    status: ComplianceStatus
}

export async function calculateCompliance(service: CloudService, day?: number): Promise<ServiceComplianceData> {
    let groupedResults: Map<string, Compliance[]>;

    if (day) {
        groupedResults = await listGroupedComplianceForDay(
            service.id,
            day
        );
    } else {
        groupedResults = await listGroupedCompliance(service.id);
    }
    const latest = await retrieveLatestCompliance(groupedResults);

    const latestValues = Array.from(latest.values());
    const numCompliant = latestValues.filter(
        (value) => isControlCompliant(value) == ComplianceStatus.Compliant
    ).length;
    const numNonCompliant = latestValues.filter(
        (value) => isControlCompliant(value) == ComplianceStatus.NonComplaint
    ).length;
    const numPending =
        (service.requirements?.requirementIds ?? []).length -
        (numCompliant + numNonCompliant);

    const chartdata = {
        labels: ["Compliant", "Not Compliant", "Pending"],
        datasets: [
            {
                backgroundColor: ["#198754", "#dc3545", "rgb(108, 117, 125)"],
                data: [numCompliant, numNonCompliant, numPending],
            },
        ],
    };

    const latestDate = Array.from(latest.values()).map(comp => new Date(comp.time)).sort((a, b) => { return a > b ? -1 : 1 })[0] ?? null;

    return {
        latest,
        latestDate,
        chartdata,
        status: isServiceCompliant(latest)
    }
}