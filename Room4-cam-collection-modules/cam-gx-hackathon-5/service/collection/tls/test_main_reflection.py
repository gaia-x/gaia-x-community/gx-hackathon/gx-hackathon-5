#!/usr/bin/env python3
from grpc_reflection.v1alpha.proto_reflection_descriptor_database import ProtoReflectionDescriptorDatabase
import grpc
import generate_protos
from main import *
from camlog import *
setuplogger()


if __name__ == '__main__':
    collector = CollectorTls()

    channel = grpc.insecure_channel('localhost:50051')
    reflection_db = ProtoReflectionDescriptorDatabase(channel)
    from google.protobuf.descriptor_pool import DescriptorPool
    desc_pool = DescriptorPool(reflection_db)
    services = reflection_db.get_services()
    info(services)
    for s in services:
        service_desc = desc_pool.FindServiceByName(s)
        info(service_desc.methods_by_name.keys())
