package integrity

import (
	"encoding/json"
	"fmt"

	"clouditor.io/clouditor/voc"
	"google.golang.org/protobuf/types/known/structpb"
)

type Value struct {
	voc.Resource

	SystemComponentsIntegrity `json:"systemComponentsIntegrity"`
}

type SystemComponentsIntegrity struct {
	Status bool `json:"status"`
}

func toStruct(v Value) (s *structpb.Value, err error) {
	var b []byte

	s = new(structpb.Value)

	if b, err = json.Marshal(v); err != nil {
		err = fmt.Errorf("JSON marshal failed: %w", err)
		return
	}
	if err = json.Unmarshal(b, &s); err != nil {
		err = fmt.Errorf("JSON unmarshal failed: %w", err)
		return
	}
	return
}

/*
func toStruct(value *structpb.Value) (conf *collection.IntegrityConfig, err error) {
	strct, err := testutil.ToStruct(value)
	if err != nil {
		return
	}

	var ok bool
	conf, ok = strct.(*collection.IntegrityConfig)
	if !ok {
		err = errors.New("")
		return
	}
	return
}

*/
